package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	httpserver "gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/left/http"
	"gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/right/assetsdata"
	"gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/right/sqlite"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/api"
	"gitlab.com/stickman_0x00/portfolio/src/internal/ports"
)

const dbPath = "db.sqlite"
const httpAddress = ":8000"

func main() {
	var DB ports.DB
	var ASSETSDATA ports.AssetsData
	var API ports.API
	var err error

	DB, err = sqlite.New(dbPath)
	if err != nil {
		panic(err)
	}

	ASSETSDATA = assetsdata.New()

	API = api.New(DB, ASSETSDATA)

	server := httpserver.New(API)

	go func() {
		err := server.Start(httpAddress)
		if err != nil && err != http.ErrServerClosed {
			log.Printf("failed to start HTTP server: %v", err)
		}
		log.Println("HTTP server stopped")
	}()

	// Listen for termination signals
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT, syscall.SIGTERM)

	log.Println("CTRL+C to stop the server")
	// Wait for termination signal
	<-signalCh

	err = server.Close()
	if err != nil {
		log.Printf("failed to stop HTTP server: %v", err)
	}

	err = DB.Close()
	if err != nil {
		log.Println("failed to close database connection")
	}
	log.Println("database connection closed")
}
