package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/fee"
)

//go:embed fees/table.sql
var sqlTableFees string

func (me SQLite) scanFee(scan func(dest ...any) error) (core.Fee, error) {
	var id int
	var amount float64
	var assetId int

	err := scan(&id, &amount, &assetId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	return fee.New(id, asset, amount), nil
}

//go:embed fees/get_all.sql
var sqlGetFees string

func (me SQLite) GetFees(transactionId int) ([]core.Fee, error) {
	rows, err := me.db.Query(sqlGetFees, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var fees []core.Fee
	for rows.Next() {
		fee, err := me.scanFee(rows.Scan)
		if err != nil {
			return nil, err
		}

		fees = append(fees, fee)
	}

	return fees, nil
}

//go:embed fees/create.sql
var sqlCreateFee string

func createFee(tx *sql.Tx, transactionId int, amount float64, asset core.Asset) (int, error) {
	res, err := tx.Exec(sqlCreateFee, transactionId, amount, asset.Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed fees/delete.sql
var sqlDeleteFee string

func (me SQLite) DeleteFee(id int) error {
	response, err := me.db.Exec(sqlDeleteFee, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "fees"}
	}

	return nil
}
