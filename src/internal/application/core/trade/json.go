package trade

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONTrade struct {
	Id         int     `json:"Id"`
	BuyAsset   int     `json:"BuyAsset"`
	BuyAmount  float64 `json:"BuyAmount"`
	SellAsset  int     `json:"SellAsset"`
	SellAmount float64 `json:"SellAmount"`
}

func (me Trade) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONTrade{
		Id:         me.id,
		BuyAsset:   me.buyAsset.Id(),
		BuyAmount:  me.buyAmount,
		SellAsset:  me.sellAsset.Id(),
		SellAmount: me.sellAmount,
	})
}

func (me *Trade) UnmarshalJSON(data []byte) error {
	var jsonT JSONTrade

	err := json.Unmarshal(data, &jsonT)
	if err != nil {
		return err
	}

	me.id = jsonT.Id
	me.buyAsset = asset.New(jsonT.BuyAsset, "", "", "", "", "")
	me.buyAmount = jsonT.BuyAmount
	me.sellAsset = asset.New(jsonT.SellAsset, "", "", "", "", "")
	me.sellAmount = jsonT.SellAmount

	return nil
}
