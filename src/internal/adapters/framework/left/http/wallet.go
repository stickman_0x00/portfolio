package httpserver

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/wallet"
)

type Wallet struct {
	Id     int           `json:"Id"`
	Name   string        `json:"Name"`
	Asset  int           `json:"Asset"`
	Assets []TotalAmount `json:"Assets"`
}

func (me HTTPServer) addWalletRoutes() {
	me.router.HandleFunc("GET /wallets", me.getWallets)
	me.router.HandleFunc("GET /wallets/{id}", me.getWallet)
	me.router.HandleFunc("POST /wallets", me.createWallet)
	me.router.HandleFunc("PUT /wallets/{id}", me.updateWallet)
	me.router.HandleFunc("DELETE /wallets/{id}", me.deleteWallet)
}

func getQueryWalletId(request *http.Request) (int, error) {
	walletIdQuery := request.URL.Query().Get("walletId")
	if walletIdQuery == "" {
		return 0, errors.New("walletId is required")
	}

	walletId, err := strconv.Atoi(walletIdQuery)
	if err != nil {
		return 0, errors.New("invalid walletId")
	}

	return walletId, nil
}

// GET /wallets
func (me HTTPServer) getWallets(response http.ResponseWriter, request *http.Request) {
	wallets, err := me.api.GetWallets()
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, wallets)
}

// GET /wallets/{id}
func (me HTTPServer) getWallet(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	wallet, err := me.api.GetWallet(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendWallet := Wallet{
		Id:    wallet.Id(),
		Name:  wallet.Name(),
		Asset: wallet.Asset().Id(),
	}

	ts, err := me.api.GetTransactions(wallet.Id())
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	total := map[int]float64{}
	for _, t := range ts {
		ta := t.TotalAmount(wallet)
		for asset, amount := range ta {
			total[asset] += amount
		}
	}

	for asset, amount := range total {
		sendWallet.Assets = append(sendWallet.Assets, TotalAmount{
			Asset:  asset,
			Amount: amount,
		})
	}

	sendResponse(response, http.StatusOK, sendWallet)
}

// POST /wallets
func (me HTTPServer) createWallet(response http.ResponseWriter, request *http.Request) {
	var requestWallet wallet.Wallet

	err := json.NewDecoder(request.Body).Decode(&requestWallet)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	createdWallet, err := me.api.CreateWallet(requestWallet)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusCreated, createdWallet)
}

// PUT /wallets/{id}
func (me HTTPServer) updateWallet(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	var requestWallet wallet.Wallet
	err = json.NewDecoder(request.Body).Decode(&requestWallet)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	updatedWallet, err := me.api.UpdateWallet(id, requestWallet)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, updatedWallet)
}

// DELETE /wallets/{id}
func (me HTTPServer) deleteWallet(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	err = me.api.DeleteWallet(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusNoContent, nil)
}
