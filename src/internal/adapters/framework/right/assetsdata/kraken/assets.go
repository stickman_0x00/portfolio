package kraken

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

func (me Kraken) GetAssets() ([]core.Asset, error) {
	resp, err := me.kraken.AssetInfo()
	if err != nil {
		return nil, err
	}

	as := []core.Asset{}
	for _, a := range resp {
		as = append(as, asset.New(0, a.Altname, "", "", "", ""))
	}

	return as, nil
}
