package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

//go:embed assets/table.sql
var sqlTableAssets string

func scanAsset(scan func(dest ...any) error) (core.Asset, error) {
	var id int
	var name string
	var code string
	var symbol string
	var source sql.NullString
	var sourceAsset sql.NullString

	err := scan(&id, &name, &code, &symbol, &source, &sourceAsset)
	if err != nil {
		return nil, err
	}

	return asset.New(id, name, code, symbol, source.String, sourceAsset.String), nil
}

//go:embed assets/get_all.sql
var sqlGetAssets string

func (me SQLite) GetAssets() ([]core.Asset, error) {
	rows, err := me.db.Query(sqlGetAssets)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var assets []core.Asset
	for rows.Next() {
		asset, err := scanAsset(rows.Scan)
		if err != nil {
			return nil, err
		}

		assets = append(assets, asset)
	}

	return assets, nil
}

//go:embed assets/get.sql
var sqlGetAsset string

func (me SQLite) GetAsset(id int) (core.Asset, error) {
	row := me.db.QueryRow(sqlGetAsset, id)

	return scanAsset(row.Scan)
}

//go:embed assets/create.sql
var sqlCreateAsset string

func (me SQLite) CreateAsset(asset core.Asset) (int, error) {
	res, err := me.db.Exec(
		sqlCreateAsset,
		asset.Name(), asset.Code(), asset.Symbol(), asset.Source(), asset.SourceAsset(),
	)
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed assets/delete.sql
var sqlDeleteAsset string

func (me SQLite) DeleteAsset(id int) error {
	response, err := me.db.Exec(sqlDeleteAsset, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "assets"}
	}

	return nil
}

//go:embed assets/update.sql
var sqlUpdateAsset string

func (me SQLite) UpdateAsset(id int, asset core.Asset) error {
	response, err := me.db.Exec(
		sqlUpdateAsset,
		sql.Named("id", id),
		sql.Named("name", asset.Name()), sql.Named("code", asset.Code()), sql.Named("symbol", asset.Symbol()),
		sql.Named("source", asset.Source()), sql.Named("sourceAsset", asset.SourceAsset()),
	)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrUpdate{Id: asset.Id(), Table: "assets"}
	}

	return nil
}
