UPDATE transactions
	SET time = $time,
		type = $type
	WHERE id = @id;