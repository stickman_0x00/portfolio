package api

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

func (me API) GetWallets() ([]core.Wallet, error) {
	return me.db.GetWallets()
}

func (me API) GetWallet(id int) (core.Wallet, error) {
	return me.db.GetWallet(id)
}

func (me API) CreateWallet(wallet core.Wallet) (core.Wallet, error) {
	id, err := me.db.CreateWallet(wallet)
	if err != nil {
		return nil, err
	}

	return me.db.GetWallet(id)
}

func (me API) UpdateWallet(id int, wallet core.Wallet) (core.Wallet, error) {
	err := me.db.UpdateWallet(id, wallet)
	if err != nil {
		return nil, err
	}

	return me.db.GetWallet(id)
}

func (me API) DeleteWallet(id int) error {
	return me.db.DeleteWallet(id)
}
