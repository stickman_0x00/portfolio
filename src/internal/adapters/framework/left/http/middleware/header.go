package middleware

import (
	"net/http"
)

func Headers(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.Header().Set("Content-Type", "application/json")

		next.ServeHTTP(response, request)
	})
}
