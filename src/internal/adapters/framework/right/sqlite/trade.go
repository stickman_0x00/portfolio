package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/trade"
)

//go:embed trades/table.sql
var sqlTableTrades string

func (me SQLite) scanTrade(scan func(dest ...any) error) (core.Trade, error) {
	var id int
	var buyAssetId int
	var buyAmount float64
	var sellAssetId int
	var sellAmount float64

	err := scan(&id, &buyAssetId, &buyAmount, &sellAssetId, &sellAmount)
	if err != nil {
		return nil, err
	}

	buyAsset, err := me.GetAsset(buyAssetId)
	if err != nil {
		return nil, err
	}
	sellAsset, err := me.GetAsset(sellAssetId)
	if err != nil {
		return nil, err
	}

	return trade.New(id, buyAsset, buyAmount, sellAsset, sellAmount), nil
}

//go:embed trades/get_all.sql
var sqlGetTrades string

func (me SQLite) GetTrades(transactionId int) ([]core.Trade, error) {
	rows, err := me.db.Query(sqlGetTrades, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var trades []core.Trade
	for rows.Next() {
		trade, err := me.scanTrade(rows.Scan)
		if err != nil {
			return nil, err
		}

		trades = append(trades, trade)
	}

	return trades, nil
}

//go:embed trades/create.sql
var sqlCreateTrade string

func createTrade(tx *sql.Tx, transactionId int, buyAsset core.Asset, buyAmount float64, sellAsset core.Asset, sellAmount float64) (int, error) {
	res, err := tx.Exec(sqlCreateTrade, transactionId, buyAsset.Id(), buyAmount, sellAsset.Id(), sellAmount)
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed trades/delete.sql
var sqlDeleteTrade string

func (me SQLite) DeleteTrade(id int) error {
	response, err := me.db.Exec(sqlDeleteTrade, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "trades"}
	}

	return nil
}
