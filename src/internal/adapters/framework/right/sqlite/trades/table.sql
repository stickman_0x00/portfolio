CREATE TABLE IF NOT EXISTS trades (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	transaction_id INTEGER NOT NULL,
	buyAsset INTEGER NOT NULL,
	buyAmount REAL NOT NULL,
	sellAsset INTEGER NOT NULL,
	sellAmount REAL NOT NULL,

	FOREIGN KEY (transaction_id) REFERENCES transactions(id),
	FOREIGN KEY (buyAsset) REFERENCES assets(id),
	FOREIGN KEY (sellAsset) REFERENCES assets(id)
);