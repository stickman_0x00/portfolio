package sqlite

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

var tables = map[string]string{
	"wallets": sqlTableWallets,
	"assets":  sqlTableAssets,

	"transaction_types": sqlTableTransactionTypes,
	"transactions":      sqlTableTransactions,

	"wallets_transactions": sqlTableWalletsTransactions,

	"deposits":  sqlTableDeposits,
	"withdraws": sqlTableWithdraws,
	"earnings":  sqlTableEarnings,
	"trades":    sqlTableTrades,
	"transfers": sqlTableTransfers,

	"fees": sqlTableFees,
}

type SQLite struct {
	db *sql.DB
}

func New(filePath string) (SQLite, error) {
	dataSourceName := "file:" + filePath + "?_foreign_keys=on"
	db, err := sql.Open("sqlite3", dataSourceName)
	if err != nil {
		return SQLite{}, err
	}

	err = createTables(db)
	if err != nil {
		return SQLite{}, err
	}

	return SQLite{
		db: db,
	}, nil
}

func createTables(db *sql.DB) error {
	db.Exec("PRAGMA foreign_keys = ON;")

	for name, table := range tables {
		_, err := db.Exec(table)
		if err != nil {
			return fmt.Errorf("failed to create table %s: %w", name, err)
		}
	}

	return nil
}

func (me SQLite) Close() error {
	return me.db.Close()
}
