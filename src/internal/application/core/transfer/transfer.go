package transfer

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

type Transfer struct {
	id       int
	asset    core.Asset
	amount   float64
	receiver core.Wallet
	sender   core.Wallet
}

func New(id int, asset core.Asset, amount float64, receiver core.Wallet, sender core.Wallet) Transfer {
	return Transfer{
		id:       id,
		asset:    asset,
		amount:   amount,
		receiver: receiver,
		sender:   sender,
	}
}

func (me Transfer) Id() int {
	return me.id
}

func (me Transfer) Asset() core.Asset {
	return me.asset
}

func (me Transfer) Amount() float64 {
	return me.amount
}

func (me Transfer) Receiver() core.Wallet {
	return me.receiver
}

func (me Transfer) Sender() core.Wallet {
	return me.sender
}
