package sqlite

import "fmt"

type ErrDelete struct {
	Id    int
	Table string
}

func (me ErrDelete) Error() string {
	return fmt.Sprintf("failed to delete %s with ID %d", me.Table, me.Id)
}

type ErrUpdate struct {
	Id    int
	Table string
}

func (me ErrUpdate) Error() string {
	return fmt.Sprintf("failed to update %s with ID %d", me.Table, me.Id)
}
