CREATE TABLE IF NOT EXISTS transfers (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	transaction_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	asset_id INTEGER NOT NULL,
	receiver_id INTEGER,
	sender_id INTEGER,

	FOREIGN KEY (transaction_id) REFERENCES transactions(id),
	FOREIGN KEY (asset_id) REFERENCES assets(id),
	FOREIGN KEY (receiver_id) REFERENCES wallets(id),
	FOREIGN KEY (sender_id) REFERENCES wallets(id)
);