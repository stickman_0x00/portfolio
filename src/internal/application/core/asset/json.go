package asset

import (
	"encoding/json"
)

type JSONAsset struct {
	Id          int    `json:"Id"`
	Name        string `json:"Name"`
	Code        string `json:"Code"`
	Symbol      string `json:"Symbol"`
	Source      string `json:"Source"`
	SourceAsset string `json:"SourceAsset"`
}

func (me Asset) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONAsset{
		Id:          me.id,
		Name:        me.name,
		Code:        me.code,
		Symbol:      me.symbol,
		Source:      me.source,
		SourceAsset: me.sourceAsset,
	})
}

func (me *Asset) UnmarshalJSON(data []byte) error {
	var jsonA JSONAsset

	err := json.Unmarshal(data, &jsonA)
	if err != nil {
		return err
	}

	me.id = jsonA.Id
	me.name = jsonA.Name
	me.code = jsonA.Code
	me.symbol = jsonA.Symbol
	me.source = jsonA.Source
	me.sourceAsset = jsonA.SourceAsset

	return nil
}
