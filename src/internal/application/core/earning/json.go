package earning

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONEarning struct {
	Id     int     `json:"Id"`
	Asset  int     `json:"Asset"`
	Amount float64 `json:"Amount"`
}

func (me Earning) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONEarning{
		Id:     me.id,
		Asset:  me.asset.Id(),
		Amount: me.amount,
	})
}

func (me *Earning) UnmarshalJSON(data []byte) error {
	var jsonE JSONEarning

	err := json.Unmarshal(data, &jsonE)
	if err != nil {
		return err
	}

	me.id = jsonE.Id
	me.asset = asset.New(jsonE.Asset, "", "", "", "", "")
	me.amount = jsonE.Amount

	return nil
}
