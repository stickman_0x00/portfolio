package ports

import (
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type DB interface {
	GetWallets() ([]core.Wallet, error)
	GetWallet(id int) (core.Wallet, error)
	CreateWallet(wallet core.Wallet) (int, error)
	DeleteWallet(id int) error
	UpdateWallet(id int, wallet core.Wallet) error

	GetAssets() ([]core.Asset, error)
	GetAsset(id int) (core.Asset, error)
	CreateAsset(asset core.Asset) (int, error)
	DeleteAsset(id int) error
	UpdateAsset(id int, asset core.Asset) error

	GetTransactions(walletId int) ([]core.Transaction, error)
	GetTransaction(id int) (core.Transaction, error)
	CreateTransaction(t core.Transaction) (core.Transaction, error)
	DeleteTransaction(id int) error
	UpdateTransaction(id int, time time.Time, typ int) (core.Transaction, error)

	Close() error
}

type AssetsData interface {
	GetSources() []string
	GetAssets(source string) ([]core.Asset, error)
}
