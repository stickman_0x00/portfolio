package httpserver

import (
	"net/http"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transaction"
)

func (me HTTPServer) addTransactionTypeRoutes() {
	me.router.HandleFunc("GET /transactionTypes", me.getTransactionTypes)
}

// GET /transaction_types
func (me HTTPServer) getTransactionTypes(response http.ResponseWriter, request *http.Request) {
	sendResponse(response, http.StatusOK, transaction.Types)
}
