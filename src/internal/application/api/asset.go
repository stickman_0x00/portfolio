package api

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

func (me API) GetAssets() ([]core.Asset, error) {
	return me.db.GetAssets()
}

func (me API) GetAsset(id int) (core.Asset, error) {
	return me.db.GetAsset(id)
}

func (me API) CreateAsset(asset core.Asset) (core.Asset, error) {
	id, err := me.db.CreateAsset(asset)
	if err != nil {
		return nil, err
	}

	asset, err = me.db.GetAsset(id)
	if err != nil {
		return nil, err
	}

	return asset, nil
}

func (me API) UpdateAsset(id int, asset core.Asset) (core.Asset, error) {
	err := me.db.UpdateAsset(id, asset)
	if err != nil {
		return nil, err
	}

	asset, err = me.db.GetAsset(id)
	if err != nil {
		return nil, err
	}

	return asset, nil
}

func (me API) DeleteAsset(id int) error {
	return me.db.DeleteAsset(id)
}
