package api

import "gitlab.com/stickman_0x00/portfolio/src/internal/ports"

type API struct {
	db         ports.DB
	assetsData ports.AssetsData
}

func New(db ports.DB, assetsData ports.AssetsData) API {
	return API{
		db:         db,
		assetsData: assetsData,
	}
}
