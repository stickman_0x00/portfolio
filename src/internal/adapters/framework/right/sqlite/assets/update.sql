UPDATE assets
	SET name = @name,
		code = @code,
		symbol = @symbol,
		source = @source,
		sourceAsset = @sourceAsset
	WHERE id = @id;