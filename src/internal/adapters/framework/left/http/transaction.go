package httpserver

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transaction"
)

type Transaction struct {
	Id          int           `json:"Id"`
	Time        int64         `json:"Time"`
	Type        int           `json:"Type"`
	TotalAmount []TotalAmount `json:"TotalAmount"`
}

type TotalAmount struct {
	Asset  int     `json:"Asset"`
	Amount float64 `json:"Amount"`
}

func (me HTTPServer) addTransactionRoutes() {
	me.router.HandleFunc("GET /transactions", me.getTransactions)
	me.router.HandleFunc("GET /transactions/{id}", me.getTransaction)
	me.router.HandleFunc("POST /transactions", me.createTransaction)
	me.router.HandleFunc("PUT /transactions/{id}", me.updateTransaction)
	me.router.HandleFunc("DELETE /transactions/{id}", me.deleteTransaction)
}

// GET /transacitons
func (me HTTPServer) getTransactions(response http.ResponseWriter, request *http.Request) {
	walletId, err := getQueryWalletId(request)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	wallet, err := me.api.GetWallet(walletId)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	transactions, err := me.api.GetTransactions(wallet.Id())
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendTransactions := make([]Transaction, len(transactions))
	for i, t := range transactions {
		sendTransactions[i] = Transaction{
			Id:   t.Id(),
			Time: t.Time().Unix(),
			Type: t.Type(),
		}
		ta := t.TotalAmount(wallet)
		for asset, amount := range ta {
			sendTransactions[i].TotalAmount = append(sendTransactions[i].TotalAmount, TotalAmount{
				Asset:  asset,
				Amount: amount,
			})
		}
	}

	sendResponse(response, http.StatusOK, sendTransactions)
}

// GET /transacitons/{id}
func (me HTTPServer) getTransaction(response http.ResponseWriter, request *http.Request) {
	transacitonId, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	transaction, err := me.api.GetTransaction(transacitonId)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, transaction)
}

// POST /transactions
func (me HTTPServer) createTransaction(response http.ResponseWriter, request *http.Request) {
	var requestTransaction transaction.Transaction

	err := json.NewDecoder(request.Body).Decode(&requestTransaction)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	createdTransaction, err := me.api.CreateTransaction(&requestTransaction)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusCreated, createdTransaction)
}

// PUT /transactions/{id}
func (me HTTPServer) updateTransaction(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	var requestTransaction transaction.Transaction
	err = json.NewDecoder(request.Body).Decode(&requestTransaction)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	updatedTransaction, err := me.api.UpdateTransaction(id, requestTransaction.Time(), requestTransaction.Type())
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, updatedTransaction)
}

// DELETE /transactions/{id}
func (me HTTPServer) deleteTransaction(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	err = me.api.DeleteTransaction(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusNoContent, nil)
}
