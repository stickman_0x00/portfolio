package earning

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

type Earning struct {
	id     int
	asset  core.Asset
	amount float64
}

func New(id int, asset core.Asset, amount float64) Earning {
	return Earning{
		id:     id,
		asset:  asset,
		amount: amount,
	}
}

func (me Earning) Id() int {
	return me.id
}

func (me Earning) Asset() core.Asset {
	return me.asset
}

func (me Earning) Amount() float64 {
	return me.amount
}
