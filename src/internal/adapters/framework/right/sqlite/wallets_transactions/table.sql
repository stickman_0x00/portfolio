-- Many-to-many relationship between wallets and transactions
CREATE TABLE IF NOT EXISTS wallets_transactions (
	wallet_id INTEGER NOT NULL,
	transaction_id INTEGER NOT NULL,

	CONSTRAINT wallets_transactions_pk PRIMARY KEY (wallet_id, transaction_id),

	FOREIGN KEY (wallet_id) REFERENCES wallets(id),
	FOREIGN KEY (transaction_id) REFERENCES transactions(id)
);