package wallet

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

type Wallet struct {
	id    int
	name  string
	asset core.Asset
}

func New(id int, name string, asset core.Asset) Wallet {
	return Wallet{
		id:    id,
		name:  name,
		asset: asset,
	}
}

func (me Wallet) Id() int {
	return me.id
}

func (me Wallet) Name() string {
	return me.name
}

func (me Wallet) Asset() core.Asset {
	return me.asset
}
