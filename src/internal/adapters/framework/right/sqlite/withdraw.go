package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/withdraw"
)

//go:embed withdraws/table.sql
var sqlTableWithdraws string

func (me SQLite) scanWithdraw(scan func(dest ...any) error) (core.Withdraw, error) {
	var id int
	var amount float64
	var assetId int

	err := scan(&id, &amount, &assetId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	return withdraw.New(id, asset, amount), nil
}

//go:embed withdraws/get_all.sql
var sqlGetWithdraws string

func (me SQLite) GetWithdraws(transactionId int) ([]core.Withdraw, error) {
	rows, err := me.db.Query(sqlGetWithdraws, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var withdraws []core.Withdraw
	for rows.Next() {
		withdraw, err := me.scanWithdraw(rows.Scan)
		if err != nil {
			return nil, err
		}

		withdraws = append(withdraws, withdraw)
	}

	return withdraws, nil
}

//go:embed withdraws/create.sql
var sqlCreateWithdraw string

func createWithdraw(tx *sql.Tx, transactionId int, amount float64, asset core.Asset) (int, error) {
	res, err := tx.Exec(sqlCreateWithdraw, transactionId, amount, asset.Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed withdraws/delete.sql
var sqlDeleteWithdraw string

func (me SQLite) DeleteWithdraw(id int) error {
	response, err := me.db.Exec(sqlDeleteWithdraw, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "withdraws"}
	}

	return nil
}
