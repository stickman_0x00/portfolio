package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/wallet"
)

//go:embed wallets/table.sql
var sqlTableWallets string

func (me SQLite) scanWallet(scan func(dest ...any) error) (core.Wallet, error) {
	var id int
	var name string
	var assetId int

	err := scan(&id, &name, &assetId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	return wallet.New(id, name, asset), nil
}

//go:embed wallets/get_all.sql
var sqlGetWallets string

func (me SQLite) GetWallets() ([]core.Wallet, error) {
	rows, err := me.db.Query(sqlGetWallets)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var wallets []core.Wallet
	for rows.Next() {
		wallet, err := me.scanWallet(rows.Scan)
		if err != nil {
			return nil, err
		}

		wallets = append(wallets, wallet)
	}

	return wallets, nil
}

//go:embed wallets/get.sql
var sqlGetWallet string

func (me SQLite) GetWallet(id int) (core.Wallet, error) {
	row := me.db.QueryRow(sqlGetWallet, id)

	return me.scanWallet(row.Scan)
}

//go:embed wallets/create.sql
var sqlCreateWallet string

func (me SQLite) CreateWallet(wallet core.Wallet) (int, error) {
	res, err := me.db.Exec(sqlCreateWallet, wallet.Name(), wallet.Asset().Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed wallets/delete.sql
var sqlDeleteWallet string

func (me SQLite) DeleteWallet(id int) error {
	response, err := me.db.Exec(sqlDeleteWallet, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "wallets"}
	}

	return nil
}

//go:embed wallets/update.sql
var sqlUpdateWallet string

func (me SQLite) UpdateWallet(id int, wallet core.Wallet) error {
	response, err := me.db.Exec(
		sqlUpdateWallet,
		sql.Named("id", id),
		sql.Named("name", wallet.Name()),
		sql.Named("asset", wallet.Asset().Id()),
	)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrUpdate{Id: id, Table: "wallets"}
	}

	return nil
}
