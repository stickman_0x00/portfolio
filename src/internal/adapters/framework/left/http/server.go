package httpserver

import (
	"net/http"

	"gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/left/http/middleware"
	"gitlab.com/stickman_0x00/portfolio/src/internal/ports"
)

type HTTPServer struct {
	router *http.ServeMux
	server *http.Server

	api ports.API
}

func New(api ports.API) HTTPServer {
	return HTTPServer{
		router: http.NewServeMux(),
		api:    api,
	}
}

func (me *HTTPServer) Start(address string) error {
	me.addWalletRoutes()
	me.addAssetRoutes()
	me.addTransactionRoutes()
	me.addTransactionTypeRoutes()
	me.addAssetsDataRoutes()

	middlewareChain := middleware.NewChain(
		middleware.Logger,
		middleware.Headers,
	)

	me.server = &http.Server{
		Addr:    address,
		Handler: middlewareChain(me.router),
	}

	return me.server.ListenAndServe()
}

func (me HTTPServer) Close() error {
	return me.server.Close()
}
