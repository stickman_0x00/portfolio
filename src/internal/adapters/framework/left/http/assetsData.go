package httpserver

import "net/http"

func (me HTTPServer) addAssetsDataRoutes() {
	me.router.HandleFunc("GET /sources", me.getSources)
	me.router.HandleFunc("GET /sources/{source}/assets", me.getSourceAssets)
}

func (me HTTPServer) getSources(response http.ResponseWriter, request *http.Request) {
	sources := me.api.GetDataSources()
	sendResponse(response, http.StatusOK, sources)
}

func (me HTTPServer) getSourceAssets(response http.ResponseWriter, request *http.Request) {
	source, err := pathValueString(request, "source")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	assets, err := me.api.GetSourceAssets(source)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, assets)
}
