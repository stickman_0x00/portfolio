SELECT *
	FROM transactions
	WHERE id IN (
		SELECT transaction_id
			FROM wallets_transactions
			WHERE wallet_id = $1
	);