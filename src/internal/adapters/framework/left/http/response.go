package httpserver

import (
	"encoding/json"
	"net/http"
)

func sendResponse(response http.ResponseWriter, status int, data any) {
	response.WriteHeader(status)

	err := json.NewEncoder(response).Encode(data)
	if err != nil {
		http.Error(response, "Internal server error", http.StatusInternalServerError)
		return
	}
}
