package sqlite

import (
	"database/sql"
	_ "embed"
)

//go:embed wallets_transactions/table.sql
var sqlTableWalletsTransactions string

//go:embed wallets_transactions/create.sql
var sqlCreateWalletTransaction string

func createWalletTransaction(tx *sql.Tx, walletId int, transactionId int) error {
	_, err := tx.Exec(sqlCreateWalletTransaction, walletId, transactionId)
	if err != nil {
		return err
	}

	return nil
}
