package httpserver

import (
	"errors"
	"net/http"
	"strconv"
)

func pathValueInt(request *http.Request, pattern string) (int, error) {
	pathValue := request.PathValue(pattern)
	id, err := strconv.Atoi(pathValue)
	if err != nil {
		return 0, errors.New("invalid " + pattern)
	}

	return id, nil
}

func pathValueString(request *http.Request, pattern string) (string, error) {
	pathValue := request.PathValue(pattern)
	if pathValue == "" {
		return "", errors.New("invalid " + pattern)
	}

	return pathValue, nil
}
