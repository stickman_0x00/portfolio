package kraken

import kraken "gitlab.com/stickman_0x00/go_kraken"

const SOURCE = "kraken"

type Kraken struct {
	kraken *kraken.Kraken
}

func New() Kraken {
	return Kraken{
		kraken: kraken.New("", ""),
	}
}
