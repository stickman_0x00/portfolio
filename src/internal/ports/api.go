package ports

import (
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type API interface {
	GetWallets() ([]core.Wallet, error)
	GetWallet(id int) (core.Wallet, error)
	CreateWallet(wallet core.Wallet) (core.Wallet, error)
	UpdateWallet(id int, wallet core.Wallet) (core.Wallet, error)
	DeleteWallet(id int) error

	GetAssets() ([]core.Asset, error)
	GetAsset(id int) (core.Asset, error)
	CreateAsset(asset core.Asset) (core.Asset, error)
	UpdateAsset(id int, asset core.Asset) (core.Asset, error)
	DeleteAsset(id int) error

	GetTransactions(walletId int) ([]core.Transaction, error)
	GetTransaction(id int) (core.Transaction, error)
	CreateTransaction(t core.Transaction) (core.Transaction, error)
	UpdateTransaction(id int, time time.Time, typ int) (core.Transaction, error)
	DeleteTransaction(id int) error

	GetDataSources() []string
	GetSourceAssets(source string) ([]core.Asset, error)
}
