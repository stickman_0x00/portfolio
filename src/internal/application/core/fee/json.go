package fee

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONFee struct {
	Id     int     `json:"Id"`
	Asset  int     `json:"Asset"`
	Amount float64 `json:"Amount"`
}

func (me Fee) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONFee{
		Id:     me.id,
		Asset:  me.asset.Id(),
		Amount: me.amount,
	})
}

func (me *Fee) UnmarshalJSON(data []byte) error {
	var jsonF JSONFee

	err := json.Unmarshal(data, &jsonF)
	if err != nil {
		return err
	}

	me.id = jsonF.Id
	me.asset = asset.New(jsonF.Asset, "", "", "", "", "")
	me.amount = jsonF.Amount

	return nil
}
