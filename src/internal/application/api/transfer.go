package api

import (
	"errors"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

func validateTransferParticipant(wallet core.Wallet, ts []core.Transfer) error {
	isReceiver := false
	isSender := false
	for _, t := range ts {
		if t.Receiver().Id() == wallet.Id() {
			isReceiver = true
		}
		if t.Sender().Id() == wallet.Id() {
			isSender = true
		}
	}

	if !isReceiver && !isSender {
		return errors.New("wallet not involved in transfer")
	}

	if isReceiver && isSender {
		return errors.New("wallet cannot be both sender and receiver")
	}

	return nil
}
