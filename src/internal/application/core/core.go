package core

import "time"

type Wallet interface {
	Id() int
	Name() string
	Asset() Asset
}

type Asset interface {
	Id() int
	Name() string
	Code() string
	Symbol() string
	Source() string
	SourceAsset() string
}

type Deposit interface {
	Id() int
	Asset() Asset
	Amount() float64
}

type Withdraw interface {
	Id() int
	Asset() Asset
	Amount() float64
}

type Transfer interface {
	Id() int
	Asset() Asset
	Amount() float64
	Receiver() Wallet
	Sender() Wallet
}

type Trade interface {
	Id() int
	BuyAsset() Asset
	BuyAmount() float64
	SellAsset() Asset
	SellAmount() float64
}

type Earning interface {
	Id() int
	Asset() Asset
	Amount() float64
}

type Fee interface {
	Id() int
	Asset() Asset
	Amount() float64
}

type Transaction interface {
	Id() int
	Time() time.Time
	Type() int
	Wallet() Wallet

	Deposits() []Deposit
	AddDeposit(deposit Deposit)
	Withdraws() []Withdraw
	AddWithdraw(withdraw Withdraw)
	Transfers() []Transfer
	AddTransfer(transfer Transfer)
	Trades() []Trade
	AddTrade(trade Trade)
	Earnings() []Earning
	AddEarning(earning Earning)

	Fees() []Fee
	AddFee(fee Fee)

	TotalAmount(wallet Wallet) map[int]float64
}
