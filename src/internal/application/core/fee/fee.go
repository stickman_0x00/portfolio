package fee

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

type Fee struct {
	id     int
	asset  core.Asset
	amount float64
}

func New(id int, asset core.Asset, amount float64) Fee {
	return Fee{
		id:     id,
		asset:  asset,
		amount: amount,
	}
}

func (me Fee) Id() int {
	return me.id
}

func (me Fee) Asset() core.Asset {
	return me.asset
}

func (me Fee) Amount() float64 {
	return me.amount
}
