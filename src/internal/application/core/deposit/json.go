package deposit

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONDeposit struct {
	Id     int     `json:"Id"`
	Asset  int     `json:"Asset"`
	Amount float64 `json:"Amount"`
}

func (me Deposit) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONDeposit{
		Id:     me.id,
		Asset:  me.asset.Id(),
		Amount: me.amount,
	})
}

func (me *Deposit) UnmarshalJSON(data []byte) error {
	var jsonD JSONDeposit

	err := json.Unmarshal(data, &jsonD)
	if err != nil {
		return err
	}

	me.id = jsonD.Id
	me.asset = asset.New(jsonD.Asset, "", "", "", "", "")
	me.amount = jsonD.Amount

	return nil
}
