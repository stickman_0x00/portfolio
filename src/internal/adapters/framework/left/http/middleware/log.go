package middleware

import (
	"log"
	"net/http"
	"time"
)

type responseWriter struct {
	status int
	http.ResponseWriter
}

func newResponse(response http.ResponseWriter) *responseWriter {
	return &responseWriter{ResponseWriter: response}
}

func (me *responseWriter) WriteHeader(code int) {
	me.status = code
	me.ResponseWriter.WriteHeader(code)
}

func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		start := time.Now()

		customResponse := newResponse(response)
		next.ServeHTTP(customResponse, request)

		log.Println(customResponse.status, request.Method, request.URL.Path, time.Since(start))
	})
}
