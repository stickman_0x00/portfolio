package withdraw

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type Withdraw struct {
	id     int
	asset  core.Asset
	amount float64
}

func New(id int, asset core.Asset, amount float64) Withdraw {
	return Withdraw{
		id:     id,
		asset:  asset,
		amount: amount,
	}
}

func (me Withdraw) Id() int {
	return me.id
}

func (me Withdraw) Asset() core.Asset {
	return me.asset
}

func (me Withdraw) Amount() float64 {
	return me.amount
}
