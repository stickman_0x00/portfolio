package transfer

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/wallet"
)

type JSONTransfer struct {
	Id       int     `json:"Id"`
	Asset    int     `json:"Asset"`
	Amount   float64 `json:"Amount"`
	Receiver int     `json:"Receiver"`
	Sender   int     `json:"Sender,omitempty"`
}

func (me Transfer) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONTransfer{
		Id:       me.id,
		Asset:    me.asset.Id(),
		Amount:   me.amount,
		Receiver: me.receiver.Id(),
		Sender:   me.sender.Id(),
	})
}

func (me *Transfer) UnmarshalJSON(data []byte) error {
	var jsonT JSONTransfer

	err := json.Unmarshal(data, &jsonT)
	if err != nil {
		return err
	}

	me.id = jsonT.Id
	me.asset = asset.New(jsonT.Asset, "", "", "", "", "")
	me.amount = jsonT.Amount
	me.receiver = wallet.New(jsonT.Receiver, "", nil)
	me.sender = wallet.New(jsonT.Sender, "", nil)

	return nil
}
