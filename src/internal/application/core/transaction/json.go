package transaction

import (
	"encoding/json"
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/deposit"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/earning"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/fee"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/trade"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transfer"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/wallet"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/withdraw"
)

type JSONTransaction struct {
	Id     int   `json:"Id"`
	Time   int64 `json:"Time"`
	Typ    int   `json:"Type"`
	Wallet int   `json:"Wallet"`

	Deposits  []deposit.Deposit   `json:"Deposits,omitempty"`
	Withdraws []withdraw.Withdraw `json:"Withdraws,omitempty"`
	Trades    []trade.Trade       `json:"Trades,omitempty"`
	Transfers []transfer.Transfer `json:"Transfers,omitempty"`
	Earnings  []earning.Earning   `json:"Earnings,omitempty"`

	Fees []fee.Fee `json:"Fees,omitempty"`
}

func (me Transaction) MarshalJSON() ([]byte, error) {
	jsonT := &JSONTransaction{
		Id:     me.id,
		Time:   me.time.Unix(),
		Typ:    me.typ.Int(),
		Wallet: me.wallet.Id(),
	}

	switch me.typ {
	case Deposit:
		for _, d := range me.deposits {
			jsonT.Deposits = append(jsonT.Deposits, d.(deposit.Deposit))
		}
	case Withdraw:
		for _, w := range me.withdraws {
			jsonT.Withdraws = append(jsonT.Withdraws, w.(withdraw.Withdraw))
		}
	case Trade:
		for _, t := range me.trades {
			jsonT.Trades = append(jsonT.Trades, t.(trade.Trade))
		}
	case Transfer:
		for _, t := range me.transfers {
			jsonT.Transfers = append(jsonT.Transfers, t.(transfer.Transfer))
		}

	case Earning:
		for _, e := range me.earnings {
			jsonT.Earnings = append(jsonT.Earnings, e.(earning.Earning))
		}
	}

	for _, f := range me.fees {
		jsonT.Fees = append(jsonT.Fees, f.(fee.Fee))
	}

	return json.Marshal(jsonT)
}

func (me *Transaction) UnmarshalJSON(data []byte) error {
	var jsonT JSONTransaction

	err := json.Unmarshal(data, &jsonT)
	if err != nil {
		return err
	}

	*me = *New(jsonT.Id, time.Unix(jsonT.Time, 0), jsonT.Typ, wallet.New(jsonT.Wallet, "", nil))

	for _, d := range jsonT.Deposits {
		me.AddDeposit(d)
	}
	for _, w := range jsonT.Withdraws {
		me.AddWithdraw(w)
	}
	for _, t := range jsonT.Trades {
		me.AddTrade(t)
	}
	for _, t := range jsonT.Transfers {
		me.AddTransfer(t)
	}
	for _, e := range jsonT.Earnings {
		me.AddEarning(e)
	}

	for _, f := range jsonT.Fees {
		me.AddFee(f)
	}

	return nil
}

type JSONType struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

func (me Type) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONType{
		Id:   me.Int(),
		Name: me.String(),
	})
}
