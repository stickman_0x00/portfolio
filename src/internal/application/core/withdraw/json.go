package withdraw

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONWithdraw struct {
	Id     int     `json:"Id"`
	Asset  int     `json:"Asset"`
	Amount float64 `json:"Amount"`
}

func (me Withdraw) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONWithdraw{
		Id:     me.id,
		Asset:  me.asset.Id(),
		Amount: me.amount,
	})
}

func (me *Withdraw) UnmarshalJSON(data []byte) error {
	var jsonW JSONWithdraw

	err := json.Unmarshal(data, &jsonW)
	if err != nil {
		return err
	}

	me.id = jsonW.Id
	me.asset = asset.New(jsonW.Asset, "", "", "", "", "")
	me.amount = jsonW.Amount

	return nil
}
