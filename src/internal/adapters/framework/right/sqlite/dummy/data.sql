INSERT INTO assets (name, code, symbol)
	VALUES
		("Euro", "EUR", "€"),
		("Dollar", "USD", "$"),
		("Pound", "GBP", "£");

INSERT INTO wallets (name, asset)
	VALUES
		("Wallet 1", 2),
		("Wallet 2", 2),
		("Wallet 3", 2);