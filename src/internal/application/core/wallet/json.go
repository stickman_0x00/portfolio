package wallet

import (
	"encoding/json"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

type JSONWallet struct {
	Id    int    `json:"Id"`
	Name  string `json:"Name"`
	Asset int    `json:"Asset"`
}

func (me Wallet) MarshalJSON() ([]byte, error) {
	return json.Marshal(&JSONWallet{
		Id:    me.id,
		Name:  me.name,
		Asset: me.asset.Id(),
	})
}

func (me *Wallet) UnmarshalJSON(data []byte) error {
	var w JSONWallet

	err := json.Unmarshal(data, &w)
	if err != nil {
		return err
	}

	me.id = w.Id
	me.name = w.Name
	me.asset = asset.New(w.Asset, "", "", "", "", "")

	return nil
}
