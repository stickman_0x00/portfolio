package transaction

import (
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type Transaction struct {
	id     int
	time   time.Time
	typ    Type
	wallet core.Wallet

	deposits  []core.Deposit
	withdraws []core.Withdraw
	trades    []core.Trade
	transfers []core.Transfer
	earnings  []core.Earning

	fees []core.Fee
}

func New(id int, tim time.Time, typ int, wallet core.Wallet) *Transaction {
	if tim.Unix() == 0 {
		tim = time.Now()
	}

	return &Transaction{
		id:     id,
		time:   tim,
		typ:    Type(typ),
		wallet: wallet,

		deposits:  []core.Deposit{},
		withdraws: []core.Withdraw{},
		trades:    []core.Trade{},
		transfers: []core.Transfer{},
		earnings:  []core.Earning{},

		fees: []core.Fee{},
	}

}

func (me Transaction) Id() int {
	return me.id
}

func (me Transaction) Time() time.Time {
	return me.time
}

func (me Transaction) Type() int {
	return me.typ.Int()
}

func (me Transaction) Wallet() core.Wallet {
	return me.wallet
}

func (me Transaction) Deposits() []core.Deposit {
	return me.deposits
}

func (me *Transaction) AddDeposit(deposit core.Deposit) {
	if me.typ != Deposit {
		return
	}

	me.deposits = append(me.deposits, deposit)
}

func (me Transaction) Withdraws() []core.Withdraw {
	return me.withdraws
}

func (me *Transaction) AddWithdraw(withdraw core.Withdraw) {
	if me.typ != Withdraw {
		return
	}

	me.withdraws = append(me.withdraws, withdraw)
}

func (me Transaction) Trades() []core.Trade {
	return me.trades
}

func (me *Transaction) AddTrade(trade core.Trade) {
	if me.typ != Trade {
		return
	}

	me.trades = append(me.trades, trade)
}

func (me Transaction) Transfers() []core.Transfer {
	return me.transfers
}

func (me *Transaction) AddTransfer(transfer core.Transfer) {
	if me.typ != Transfer {
		return
	}

	me.transfers = append(me.transfers, transfer)
}

func (me Transaction) Earnings() []core.Earning {
	return me.earnings
}

func (me *Transaction) AddEarning(earning core.Earning) {
	if me.typ != Earning {
		return
	}

	me.earnings = append(me.earnings, earning)
}

func (me Transaction) Fees() []core.Fee {
	return me.fees
}

func (me *Transaction) AddFee(fee core.Fee) {
	me.fees = append(me.fees, fee)
}

func (me *Transaction) TotalAmount(wallet core.Wallet) map[int]float64 {
	if me.wallet.Id() != wallet.Id() && me.typ != Transfer {
		return nil
	}

	amounts := map[int]float64{}

	switch me.typ {
	case Deposit:
		for _, d := range me.deposits {
			amounts[d.Asset().Id()] += d.Amount()
		}
	case Withdraw:
		for _, w := range me.withdraws {
			amounts[w.Asset().Id()] -= w.Amount()
		}
	case Trade:
		for _, t := range me.trades {
			amounts[t.BuyAsset().Id()] += t.BuyAmount()
			amounts[t.SellAsset().Id()] -= t.SellAmount()
		}
	case Transfer:
		for _, t := range me.transfers {
			if t.Sender().Id() == wallet.Id() {
				amounts[t.Asset().Id()] -= t.Amount()
			}
			if t.Receiver().Id() == wallet.Id() {
				amounts[t.Asset().Id()] += t.Amount()
			}
		}

		if me.wallet.Id() != wallet.Id() { // if wallet is not the owner of the transaction
			return amounts
		}
	case Earning:
		for _, e := range me.earnings {
			amounts[e.Asset().Id()] += e.Amount()
		}
	}

	for _, f := range me.fees {
		amounts[f.Asset().Id()] -= f.Amount()
	}

	return amounts
}
