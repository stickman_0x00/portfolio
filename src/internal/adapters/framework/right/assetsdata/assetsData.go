package assetsdata

import (
	"fmt"

	"gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/right/assetsdata/kraken"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type AssetsData struct {
	sources map[string]Source
}

func New() AssetsData {
	return AssetsData{
		sources: map[string]Source{
			KRAKEN: kraken.New(),
		},
	}
}

func (me AssetsData) GetAssets(source string) ([]core.Asset, error) {
	if s, ok := me.sources[source]; ok {
		return s.GetAssets()
	}

	return nil, fmt.Errorf("source not found")
}
