package httpserver

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/asset"
)

func (me HTTPServer) addAssetRoutes() {
	me.router.HandleFunc("GET /assets", me.getAssets)
	me.router.HandleFunc("GET /assets/{id}", me.getAsset)
	me.router.HandleFunc("POST /assets", me.createAsset)
	me.router.HandleFunc("PUT /assets/{id}", me.updateAsset)
	me.router.HandleFunc("DELETE /assets/{id}", me.deleteAsset)
}

// GET /assets
func (me HTTPServer) getAssets(response http.ResponseWriter, request *http.Request) {
	assets, err := me.api.GetAssets()
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, assets)
}

// GET /assets/{id}
func (me HTTPServer) getAsset(response http.ResponseWriter, request *http.Request) {
	idPath := request.PathValue("id")
	id, err := strconv.Atoi(idPath)
	if err != nil {
		http.Error(response, "invalid id", http.StatusBadRequest)
		return
	}

	asset, err := me.api.GetAsset(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, asset)
}

// POST /assets
func (me HTTPServer) createAsset(response http.ResponseWriter, request *http.Request) {
	var requestAsset asset.Asset

	err := json.NewDecoder(request.Body).Decode(&requestAsset)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	createdAsset, err := me.api.CreateAsset(requestAsset)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, createdAsset)
}

// PUT /assets/{id}
func (me HTTPServer) updateAsset(response http.ResponseWriter, request *http.Request) {
	id, err := pathValueInt(request, "id")
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}

	var requestAsset asset.Asset
	err = json.NewDecoder(request.Body).Decode(&requestAsset)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
		return
	}
	defer request.Body.Close()

	updatedAsset, err := me.api.UpdateAsset(id, requestAsset)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, updatedAsset)
}

// DELETE /assets/{id}
func (me HTTPServer) deleteAsset(response http.ResponseWriter, request *http.Request) {
	idPath := request.PathValue("id")
	id, err := strconv.Atoi(idPath)
	if err != nil {
		http.Error(response, "invalid id", http.StatusBadRequest)
		return
	}

	err = me.api.DeleteAsset(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	sendResponse(response, http.StatusOK, nil)
}
