package sqlite

import (
	"database/sql"
	_ "embed"
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/deposit"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/earning"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/fee"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/trade"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transaction"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transfer"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/withdraw"
)

//go:embed transactions/table.sql
var sqlTableTransactions string

func (me SQLite) scanTransaction(scan func(dest ...any) error) (core.Transaction, error) {
	var id int
	var timeUnix int64
	var typ int
	var walletId int

	err := scan(&id, &timeUnix, &typ, &walletId)
	if err != nil {
		return nil, err
	}

	wallet, err := me.GetWallet(walletId)
	if err != nil {
		return nil, err
	}

	return transaction.New(id, time.Unix(timeUnix, 0), typ, wallet), nil
}

//go:embed transactions/get_all.sql
var sqlGetTransactions string

func (me SQLite) GetTransactions(walletId int) ([]core.Transaction, error) {
	rows, err := me.db.Query(sqlGetTransactions, walletId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var transactions []core.Transaction
	for rows.Next() {
		t, err := me.scanTransaction(rows.Scan)
		if err != nil {
			return nil, err
		}

		switch t.Type() {
		case transaction.Deposit.Int():
			deposits, err := me.GetDeposits(t.Id())
			if err != nil {
				return nil, err
			}
			for _, d := range deposits {
				t.AddDeposit(d)
			}
		case transaction.Withdraw.Int():
			withdraws, err := me.GetWithdraws(t.Id())
			if err != nil {
				return nil, err
			}
			for _, w := range withdraws {
				t.AddWithdraw(w)
			}
		case transaction.Earning.Int():
			earnings, err := me.GetEarnings(t.Id())
			if err != nil {
				return nil, err
			}
			for _, e := range earnings {
				t.AddEarning(e)
			}
		case transaction.Trade.Int():
			trades, err := me.GetTrades(t.Id())
			if err != nil {
				return nil, err
			}
			for _, tr := range trades {
				t.AddTrade(tr)
			}
		case transaction.Transfer.Int():
			transfers, err := me.GetTransfers(t.Id())
			if err != nil {
				return nil, err
			}
			for _, tr := range transfers {
				t.AddTransfer(tr)
			}
		}

		fees, err := me.GetFees(t.Id())
		if err != nil {
			return nil, err
		}
		for _, f := range fees {
			t.AddFee(f)
		}

		transactions = append(transactions, t)
	}

	return transactions, nil
}

//go:embed transactions/get.sql
var sqlGetTransaction string

func (me SQLite) GetTransaction(id int) (core.Transaction, error) {
	row := me.db.QueryRow(sqlGetTransaction, id)
	t, err := me.scanTransaction(row.Scan)
	if err != nil {
		return nil, err
	}

	switch t.Type() {
	case transaction.Deposit.Int():
		deposits, err := me.GetDeposits(t.Id())
		if err != nil {
			return nil, err
		}
		for _, d := range deposits {
			t.AddDeposit(d)
		}
	case transaction.Withdraw.Int():
		withdraws, err := me.GetWithdraws(t.Id())
		if err != nil {
			return nil, err
		}
		for _, w := range withdraws {
			t.AddWithdraw(w)
		}
	case transaction.Earning.Int():
		earnings, err := me.GetEarnings(t.Id())
		if err != nil {
			return nil, err
		}
		for _, e := range earnings {
			t.AddEarning(e)
		}
	case transaction.Trade.Int():
		trades, err := me.GetTrades(t.Id())
		if err != nil {
			return nil, err
		}
		for _, tr := range trades {
			t.AddTrade(tr)
		}
	case transaction.Transfer.Int():
		transfers, err := me.GetTransfers(t.Id())
		if err != nil {
			return nil, err
		}
		for _, tr := range transfers {
			t.AddTransfer(tr)
		}
	}

	fees, err := me.GetFees(t.Id())
	if err != nil {
		return nil, err
	}
	for _, f := range fees {
		t.AddFee(f)
	}

	return t, nil
}

//go:embed transactions/create.sql
var sqlCreateTransaction string

func (me SQLite) CreateTransaction(t core.Transaction) (core.Transaction, error) {
	tx, err := me.db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	res, err := tx.Exec(sqlCreateTransaction, t.Time().Unix(), t.Type(), t.Wallet().Id())
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	err = createWalletTransaction(tx, t.Wallet().Id(), int(id))
	if err != nil {
		return nil, err
	}

	newT := transaction.New(int(id), t.Time(), t.Type(), t.Wallet())

	switch t.Type() {
	case transaction.Deposit.Int():
		for _, d := range t.Deposits() {
			id, err := createDeposit(tx, newT.Id(), d.Amount(), d.Asset())
			if err != nil {
				return nil, err
			}

			newT.AddDeposit(deposit.New(int(id), d.Asset(), d.Amount()))
		}
	case transaction.Withdraw.Int():
		for _, w := range t.Withdraws() {
			id, err := createWithdraw(tx, newT.Id(), w.Amount(), w.Asset())
			if err != nil {
				return nil, err
			}

			newT.AddWithdraw(withdraw.New(int(id), w.Asset(), w.Amount()))
		}
	case transaction.Earning.Int():
		for _, e := range t.Earnings() {
			id, err := createEarning(tx, newT.Id(), e.Amount(), e.Asset())
			if err != nil {
				return nil, err
			}

			newT.AddEarning(earning.New(int(id), e.Asset(), e.Amount()))
		}
	case transaction.Trade.Int():
		for _, tr := range t.Trades() {
			id, err := createTrade(tx, newT.Id(), tr.BuyAsset(), tr.BuyAmount(), tr.SellAsset(), tr.SellAmount())
			if err != nil {
				return nil, err
			}

			newT.AddTrade(trade.New(int(id), tr.BuyAsset(), tr.BuyAmount(), tr.SellAsset(), tr.SellAmount()))
		}
	case transaction.Transfer.Int():
		otherWalletId := map[int]any{}
		for _, tr := range t.Transfers() {
			id, err := createTransfer(tx, newT.Id(), tr.Amount(), tr.Asset(), tr.Receiver(), tr.Sender())
			if err != nil {
				return nil, err
			}

			if tr.Sender().Id() != 0 && tr.Sender().Id() != t.Wallet().Id() {
				otherWalletId[tr.Sender().Id()] = nil
			}

			if tr.Receiver().Id() != 0 && tr.Receiver().Id() != t.Wallet().Id() {
				otherWalletId[tr.Receiver().Id()] = nil
			}

			newT.AddTransfer(transfer.New(int(id), tr.Asset(), tr.Amount(), tr.Receiver(), tr.Sender()))
		}

		for id := range otherWalletId {
			err = createWalletTransaction(tx, id, newT.Id())
			if err != nil {
				return nil, err
			}
		}
	}

	for _, f := range t.Fees() {
		id, err := createFee(tx, newT.Id(), f.Amount(), f.Asset())
		if err != nil {
			return nil, err
		}
		newT.AddFee(fee.New(int(id), f.Asset(), f.Amount()))
	}

	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	return newT, nil
}

//go:embed transactions/delete.sql
var sqlDeleteTransaction string

func (me SQLite) DeleteTransaction(id int) error {
	response, err := me.db.Exec(sqlDeleteTransaction, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "transactions"}
	}

	return nil
}

//go:embed transactions/update.sql
var sqlUpdateTransaction string

func (me SQLite) UpdateTransaction(id int, time time.Time, typ int) (core.Transaction, error) {
	response, err := me.db.Exec(
		sqlUpdateTransaction,
		sql.Named("id", id),
		sql.Named("time", time.Unix()), sql.Named("type", typ),
	)
	if err != nil {
		return nil, err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return nil, err
	}

	if rowsAffected == 0 {
		return nil, ErrUpdate{Id: id, Table: "transactions"}
	}

	return me.GetTransaction(id)
}
