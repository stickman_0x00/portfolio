package deposit

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

type Deposit struct {
	id     int
	asset  core.Asset
	amount float64
}

func New(id int, asset core.Asset, amount float64) Deposit {
	return Deposit{
		id:     id,
		asset:  asset,
		amount: amount,
	}
}

func (me Deposit) Id() int {
	return me.id
}

func (me Deposit) Asset() core.Asset {
	return me.asset
}

func (me Deposit) Amount() float64 {
	return me.amount
}
