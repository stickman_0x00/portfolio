package middleware

import (
	"net/http"
)

type middleware func(http.Handler) http.Handler

func NewChain(middlewares ...middleware) middleware {
	return func(handler http.Handler) http.Handler {
		length := len(middlewares) - 1

		for i := range middlewares {
			handler = middlewares[length-i](handler)
		}

		return handler
	}
}
