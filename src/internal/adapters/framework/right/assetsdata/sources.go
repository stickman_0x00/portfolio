package assetsdata

import (
	"gitlab.com/stickman_0x00/portfolio/src/internal/adapters/framework/right/assetsdata/kraken"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
)

const (
	KRAKEN = kraken.SOURCE
)

type Source interface {
	GetAssets() ([]core.Asset, error)
}

func (me AssetsData) GetSources() []string {
	keys := make([]string, 0, len(me.sources))

	for k := range me.sources {
		keys = append(keys, k)
	}

	return keys
}
