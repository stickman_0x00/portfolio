module gitlab.com/stickman_0x00/portfolio

go 1.22.4

require github.com/mattn/go-sqlite3 v1.14.22

require gitlab.com/stickman_0x00/go_kraken v0.0.0-20240616170519-0357fa45f653
