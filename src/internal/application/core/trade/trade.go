package trade

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

type Trade struct {
	id         int
	buyAsset   core.Asset
	buyAmount  float64
	sellAsset  core.Asset
	sellAmount float64
}

func New(id int, buyAsset core.Asset, buyAmount float64, sellAsset core.Asset, sellAmount float64) Trade {
	return Trade{
		id:         id,
		buyAsset:   buyAsset,
		buyAmount:  buyAmount,
		sellAsset:  sellAsset,
		sellAmount: sellAmount,
	}
}

func (me Trade) Id() int {
	return me.id
}

func (me Trade) BuyAsset() core.Asset {
	return me.buyAsset
}

func (me Trade) BuyAmount() float64 {
	return me.buyAmount
}

func (me Trade) SellAsset() core.Asset {
	return me.sellAsset
}

func (me Trade) SellAmount() float64 {
	return me.sellAmount
}
