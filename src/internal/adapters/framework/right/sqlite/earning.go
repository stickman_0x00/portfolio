package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/earning"
)

//go:embed earnings/table.sql
var sqlTableEarnings string

func (me SQLite) scanEarning(scan func(dest ...any) error) (core.Earning, error) {
	var id int
	var amount float64
	var assetId int

	err := scan(&id, &amount, &assetId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	return earning.New(id, asset, amount), nil
}

//go:embed earnings/get_all.sql
var sqlGetEarnings string

func (me SQLite) GetEarnings(transactionId int) ([]core.Earning, error) {
	rows, err := me.db.Query(sqlGetEarnings, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var earnings []core.Earning
	for rows.Next() {
		earning, err := me.scanEarning(rows.Scan)
		if err != nil {
			return nil, err
		}

		earnings = append(earnings, earning)
	}

	return earnings, nil
}

//go:embed earnings/create.sql
var sqlCreateEarning string

func createEarning(tx *sql.Tx, transactionId int, amount float64, asset core.Asset) (int, error) {
	res, err := tx.Exec(sqlCreateEarning, transactionId, amount, asset.Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed earnings/delete.sql
var sqlDeleteEarning string

func (me SQLite) DeleteEarning(id int) error {
	response, err := me.db.Exec(sqlDeleteEarning, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "earnings"}
	}

	return nil
}
