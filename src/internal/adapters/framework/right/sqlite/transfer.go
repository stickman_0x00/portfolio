package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transfer"
)

//go:embed transfers/table.sql
var sqlTableTransfers string

func (me SQLite) scanTransfer(scan func(dest ...any) error) (core.Transfer, error) {
	var id int
	var assetId int
	var amount float64
	var receiverId int
	var senderId int

	err := scan(&id, &amount, &assetId, &receiverId, &senderId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	receiver, err := me.GetWallet(receiverId)
	if err != nil {
		return nil, err
	}

	sender, err := me.GetWallet(senderId)
	if err != nil {
		return nil, err
	}

	return transfer.New(id, asset, amount, receiver, sender), nil
}

//go:embed transfers/get_all.sql
var sqlGetTransfers string

func (me SQLite) GetTransfers(transactionId int) ([]core.Transfer, error) {
	rows, err := me.db.Query(sqlGetTransfers, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var transfers []core.Transfer
	for rows.Next() {
		transfer, err := me.scanTransfer(rows.Scan)
		if err != nil {
			return nil, err
		}

		transfers = append(transfers, transfer)
	}

	return transfers, nil
}

//go:embed transfers/create.sql
var sqlCreateTransfer string

func createTransfer(tx *sql.Tx, transactionId int, amount float64, asset core.Asset, receiver core.Wallet, sender core.Wallet) (int, error) {
	res, err := tx.Exec(sqlCreateTransfer, transactionId, amount, asset.Id(), receiver.Id(), sender.Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed transfers/delete.sql
var sqlDeleteTransfer string

func (me SQLite) DeleteTransfer(id int) error {
	response, err := me.db.Exec(sqlDeleteTransfer, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "transfers"}
	}

	return nil
}
