package sqlite

import (
	"database/sql"
	_ "embed"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/deposit"
)

//go:embed deposits/table.sql
var sqlTableDeposits string

func (me SQLite) scanDeposit(scan func(dest ...any) error) (core.Deposit, error) {
	var id int
	var amount float64
	var assetId int

	err := scan(&id, &amount, &assetId)
	if err != nil {
		return nil, err
	}

	asset, err := me.GetAsset(assetId)
	if err != nil {
		return nil, err
	}

	return deposit.New(id, asset, amount), nil
}

//go:embed deposits/get_all.sql
var sqlGetDeposits string

func (me SQLite) GetDeposits(transactionId int) ([]core.Deposit, error) {
	rows, err := me.db.Query(sqlGetDeposits, transactionId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var deposits []core.Deposit
	for rows.Next() {
		deposit, err := me.scanDeposit(rows.Scan)
		if err != nil {
			return nil, err
		}

		deposits = append(deposits, deposit)
	}

	return deposits, nil
}

//go:embed deposits/create.sql
var sqlCreateDeposit string

func createDeposit(tx *sql.Tx, transactionId int, amount float64, asset core.Asset) (int, error) {
	res, err := tx.Exec(sqlCreateDeposit, transactionId, amount, asset.Id())
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

//go:embed deposits/delete.sql
var sqlDeleteDeposit string

func (me SQLite) DeleteDeposit(id int) error {
	response, err := me.db.Exec(sqlDeleteDeposit, id)
	if err != nil {
		return err
	}

	rowsAffected, err := response.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDelete{Id: id, Table: "deposits"}
	}

	return nil
}
