package api

import "gitlab.com/stickman_0x00/portfolio/src/internal/application/core"

func (me API) GetDataSources() []string {
	return me.assetsData.GetSources()
}

func (me API) GetSourceAssets(source string) ([]core.Asset, error) {
	return me.assetsData.GetAssets(source)
}
