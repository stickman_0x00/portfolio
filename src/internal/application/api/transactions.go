package api

import (
	"errors"
	"time"

	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core"
	"gitlab.com/stickman_0x00/portfolio/src/internal/application/core/transaction"
)

func (me API) GetTransactions(walletId int) ([]core.Transaction, error) {
	return me.db.GetTransactions(walletId)
}

func (me API) GetTransaction(id int) (core.Transaction, error) {
	return me.db.GetTransaction(id)
}

func (me API) CreateTransaction(t core.Transaction) (core.Transaction, error) {
	if t.Wallet() == nil {
		return nil, errors.New("wallet is required")
	}

	_, err := me.db.GetWallet(t.Wallet().Id())
	if err != nil {
		return nil, errors.New("wallet not found")
	}

	if t.Type() == transaction.Transfer.Int() {
		err := validateTransferParticipant(t.Wallet(), t.Transfers())
		if err != nil {
			return nil, err
		}
	}

	createdT, err := me.db.CreateTransaction(t)
	if err != nil {
		return nil, err
	}

	return createdT, nil
}

func (me API) UpdateTransaction(id int, time time.Time, typ int) (core.Transaction, error) {
	return me.db.UpdateTransaction(id, time, typ)
}

func (me API) DeleteTransaction(id int) error {
	return me.db.DeleteTransaction(id)
}
