CREATE TABLE IF NOT EXISTS deposits (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	transaction_id INTEGER NOT NULL,
	amount REAL NOT NULL,
	asset_id INTEGER NOT NULL,

	FOREIGN KEY (transaction_id) REFERENCES transactions(id),
	FOREIGN KEY (asset_id) REFERENCES assets(id)
);