-- Transaction types "ENUM"
CREATE TABLE IF NOT EXISTS transaction_types (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL
);

INSERT OR IGNORE
	INTO transaction_types (id, name)
	VALUES
		(0,'Deposit'),
		(1,'Withdraw'),
		(2,'Transfer'),
		(3,'Trade'),
		(4,'Earning');