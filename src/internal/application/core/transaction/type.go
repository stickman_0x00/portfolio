package transaction

type Type int

const (
	Deposit Type = iota
	Withdraw
	Transfer
	Trade
	Earning
)

var Types = []Type{
	Deposit,
	Withdraw,
	Transfer,
	Trade,
	Earning,
}

func (me Type) String() string {
	return [...]string{
		"Deposit",
		"Withdraw",
		"Transfer",
		"Trade",
		"Earning",
	}[me]
}

func (me Type) Int() int {
	return int(me)
}
