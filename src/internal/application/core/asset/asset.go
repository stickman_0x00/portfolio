package asset

type Asset struct {
	id          int
	name        string
	code        string
	symbol      string
	source      string
	sourceAsset string
}

func New(id int, name, code, symbol string, source string, sourceAsset string) Asset {
	return Asset{
		id:          id,
		name:        name,
		code:        code,
		symbol:      symbol,
		source:      source,
		sourceAsset: sourceAsset,
	}
}

func (me Asset) Id() int {
	return me.id
}

func (me Asset) Name() string {
	return me.name
}

func (me Asset) Code() string {
	return me.code
}

func (me Asset) Symbol() string {
	return me.symbol
}

func (me Asset) Source() string {
	return me.source
}

func (me Asset) SourceAsset() string {
	return me.sourceAsset
}
